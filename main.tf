# 1. Create vpc

resource "aws_vpc" "production_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "production"
  }
}

# 2. Create Internet Gateway (to send trafic out)

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.production_vpc.id
}

# 3. Create Subnet's

resource "aws_subnet" "public_subnet_1" {
  vpc_id     = aws_vpc.production_vpc.id
  cidr_block = "10.0.1.0/24"
  #map_public_ip_on_launch = true
  availability_zone = var.aws_zone1
}

resource "aws_subnet" "public_subnet_2" {
  vpc_id     = aws_vpc.production_vpc.id
  cidr_block = "10.0.2.0/24"
  #map_public_ip_on_launch = true
  availability_zone = var.aws_zone2
}

resource "aws_subnet" "private_subnet_1" {
  vpc_id     = aws_vpc.production_vpc.id
  cidr_block = "10.0.3.0/24"
}

resource "aws_subnet" "private_subnet_2" {
  vpc_id     = aws_vpc.production_vpc.id
  cidr_block = "10.0.4.0/24"
}

# 4. Create NAT Gateway

resource "aws_eip" "nat_eip_1" {
  domain = "vpc"
}

resource "aws_nat_gateway" "nat_gateway_1" {
  allocation_id = aws_eip.nat_eip_1.id
  subnet_id     = aws_subnet.public_subnet_1.id
}

resource "aws_eip" "nat_eip_2" {
  domain = "vpc"
}

resource "aws_nat_gateway" "nat_gateway_2" {
  allocation_id = aws_eip.nat_eip_2.id
  subnet_id     = aws_subnet.public_subnet_2.id
}

# 5. Create Custom Route Table

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.production_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
}

resource "aws_route_table" "private_rt_1" {
  vpc_id = aws_vpc.production_vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_1.id
  }
}

resource "aws_route_table" "private_rt_2" {
  vpc_id = aws_vpc.production_vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_2.id
  }
}

# 6. Associate subnet with Route Table

resource "aws_route_table_association" "public_rta_1" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table_association" "public_rta_2" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table_association" "private_rta_1" {
  subnet_id      = aws_subnet.private_subnet_1.id
  route_table_id = aws_route_table.private_rt_1.id
}

resource "aws_route_table_association" "private_rta_2" {
  subnet_id      = aws_subnet.private_subnet_2.id
  route_table_id = aws_route_table.private_rt_2.id
}



# 7. Create key-value pair for port 22
# 3 resources down toggether, create keypair
resource "aws_key_pair" "TF_aws_public_key" {
  key_name   = join("_", ["TF", var.project_short_name, "aws_public_key"])
  public_key = tls_private_key.rsa.public_key_openssh
}

resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "TF_aws_private_key" {
  content  = tls_private_key.rsa.private_key_pem
  filename = join("_", ["TF", var.project_short_name, "aws_private_key.pem"])
  file_permission = 0400
}

# 8. Launch Template

resource "aws_launch_template" "aws_lt" {
  image_id      = var.instances_image_id
  instance_type = var.instances_type

  key_name = join("_", ["TF", var.project_short_name, "aws_public_key"])

  vpc_security_group_ids = [aws_security_group.allow_web.id]

  user_data = filebase64("${path.module}/system_first_run.sh")

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "webserver"
    }
  }
}

# 9. Application Load Balancer
resource "aws_lb" "my_lb" {
  name               = "my-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.load_balancer_sg.id]
  subnets            = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id]

  enable_deletion_protection = false
  enable_cross_zone_load_balancing = true
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.my_lb.arn
  # 1 option: Load-balance-HTTPS to EC2-HTTP
  port              = "80"
  protocol          = "HTTP"

  # 2 option: Load-balance-HTTPS to EC2-HTTPS
#   port              = "443"
#   protocol          = "HTTPS"
#   ssl_policy        = "ELBSecurityPolicy-2016-08"
#   certificate_arn   = "arn:aws:iam::123456789012:server-certificate/certName"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_tg.arn
  }
}

resource "aws_lb_target_group" "lb_tg" {
  name     = var.project_short_name
  port     = 80                 # for end to end HTTPS change here to: 433
  protocol = "HTTP"             # for end to end HTTPS change here to: HTTPS
  vpc_id   = aws_vpc.production_vpc.id

  # Health check configuration
  health_check {
    enabled             = true
    interval            = 30
    path                = "/"
    port                = "traffic-port"
    timeout             = 5
    healthy_threshold   = 3
    unhealthy_threshold = 3
    protocol            = "HTTP"
    matcher             = "200-399"
  }
}

# 10. Auto Scaling Group
resource "aws_autoscaling_group" "asg" {
  desired_capacity   = 2
  max_size           = 5
  min_size           = 1

  launch_template {
    id      = aws_launch_template.aws_lt.id
    version = "$Latest"
  }

  vpc_zone_identifier = [aws_subnet.private_subnet_1.id, aws_subnet.private_subnet_2.id]
  target_group_arns   = [aws_lb_target_group.lb_tg.arn]
}

resource "aws_autoscaling_attachment" "asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.asg.name
  lb_target_group_arn   = aws_lb_target_group.lb_tg.arn
}


output "lb_dns_name" {
  description = "You can visit your instances thru load-balance (The DNS name of the LB):"
  value       = aws_lb.my_lb.dns_name
}

# if you want to terraform open your browser after successful apply
# resource "null_resource" "open_browser" {
#   provisioner "local-exec" {
#     command = "open http://${aws_lb.my_lb.dns_name}"
#   }
#   triggers = {
#     alb_dns_name = aws_lb.my_lb.dns_name
#   }
# }