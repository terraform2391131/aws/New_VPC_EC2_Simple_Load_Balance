variable "aws_region"{
    description = "AWS server region"
    type = string
    default = "eu-central-1"
}

variable "aws_zone1"{
    description = "AWS server zone"
    type = string
    default = "eu-central-1a"
}

variable "aws_zone2"{
    description = "AWS server zone"
    type = string
    default = "eu-central-1b"
}

variable "project_short_name" {
    description = "Project ShortName for project variables"
    type = string
    default = "project1"
}

variable "instances_image_id" {
    description = "AMI image id"
    type = string
    default = "ami-061afb021813a8927"
}

variable "instances_type" {
    description = "AMI instances type"
    type = string
    default = "t3.micro"
}


provider "aws" {
    profile = "default"
    region = var.aws_region
}