# New_VPC_EC2_Simple_Load_Balance
<br>
Simple example how to create Load-balancer with two (default) instances.<br>
Without Jump Host! If you need to connect to one of instances you need to<br>
manualy in AWS Menagment Console / EC2 / Elastic IPs<br>
Allocate and Associate Elastic IP address to an EC2 instance.
Then you can connect to it thru SSH 22.
<br>
What's included:<br>
<br>
New VPC<br>
2x EC2 instance with Ubuntu and apache2<br>
NAT Gateway for thoes two EC2 instances to talk to internet <br>
Load-balancer (80 or 433)<br>
Load-balancer - Health check<br>
2x Route Table, one for Load Balancer, one for EC2 instances<br>
Key value pairs for SSH 22<br>
Auto Scaling Group<br>
<br>
# Edit all settings only in "terraform.tfvars" file
